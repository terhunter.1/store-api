﻿namespace Store.API.Codes
{
    public enum ErrorCodes : uint
    {
        Unknown = 0,
        IdNotFound = 1,
        DepartmentNotFound = 2,
        SizeNotFound = 3,
        CategoryNotFound = 4,
        ColorNotFound = 5,
        MerchandiseNotFound = 6,
        SalesmanNotFound = 7,
        NotEnoughQuantity = 8,
        Unauthorized = 9,
    }
}
