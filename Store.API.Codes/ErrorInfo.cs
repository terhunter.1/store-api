﻿namespace Store.API.Codes
{
    public class ErrorInfo
    {
        public string Message { get; set; }

        public uint Code { get; set; }
    }
}
