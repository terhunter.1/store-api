﻿using System.Collections.Generic;

namespace Store.API.Codes
{
    /// <summary>
    ///  Статический класс содержащий словарь с описанием ошибок
    /// </summary>
    public static class ErrorDictionary
    {
        public static readonly Dictionary<ErrorCodes, string> _dictionary = new()
        {
            { ErrorCodes.CategoryNotFound, "CategoryId not found" },
            { ErrorCodes.IdNotFound, "Id not found" },
            { ErrorCodes.ColorNotFound, "ColorId not found" },
            { ErrorCodes.DepartmentNotFound, "DepartmentId not found" },
            { ErrorCodes.MerchandiseNotFound, "MerchandiseId not found" },
            { ErrorCodes.SalesmanNotFound, "SalesmanId not found" },
            { ErrorCodes.SizeNotFound, "SizeId not found" },
            { ErrorCodes.Unknown, "Internal error" },
            { ErrorCodes.NotEnoughQuantity, "There is not enough product in stock" },
            { ErrorCodes.Unauthorized, "Incorrect login or password" }
        };

    }
}
