﻿using System;

namespace Store.API.Codes
{
    public class StoreException : Exception
    {
        public ErrorCodes _code;
        
        public StoreException(ErrorCodes code) : base(code.ToString())
        {
            _code = code;
        }
        /// <summary>
        /// Парсинг ошибки в объект
        /// </summary>
        public ErrorInfo GetError()
        {
            return new ErrorInfo()
            {
                Message = ErrorDictionary._dictionary[_code],
                Code = (uint)_code,
            };

        }
    }
}
