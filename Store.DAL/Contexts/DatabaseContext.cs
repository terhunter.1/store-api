﻿using Microsoft.EntityFrameworkCore;
using Store.DAL.Entities;

namespace Store.DAL.Contexts
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<DepartmentEntity> Departments { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<ColorEntity> Colors { get; set; }
        public DbSet<SalesmanEntity> Salesmen { get; set; }
        public DbSet<SizeEntity> Sizes { get; set; }
        public DbSet<MerchandiseEntity> Merchandises { get; set; }
        public DbSet<SalesEntity> Sales { get; set; }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
