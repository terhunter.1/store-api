﻿using System.ComponentModel.DataAnnotations;

namespace Store.DAL.Entities
{
    public class ColorEntity
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
    }
}
