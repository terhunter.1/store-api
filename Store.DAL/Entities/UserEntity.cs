﻿using System.ComponentModel.DataAnnotations;

namespace Store.DAL.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string Login { get; set; }

        [Required]
        [MaxLength(16)]
        public string Password { get; set; }

        [Required]
        public string Permission { get; set; }
    }
}
