﻿namespace Store.DAL.Entities
{
    public class SalesEntity
    {
        public int Id { get; set; }

        public uint Quantity { get; set; }

        public virtual MerchandiseEntity Merchandise { get; set; }
        public virtual SalesmanEntity Salesman { get; set; }
    }
}
