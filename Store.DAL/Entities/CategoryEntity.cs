﻿using System.ComponentModel.DataAnnotations;

namespace Store.DAL.Entities
{
    public class CategoryEntity
    {
        public int Id { get; set; }

        public virtual DepartmentEntity Department { get; set; }

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
    }
}
