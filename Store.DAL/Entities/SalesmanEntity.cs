﻿using System.ComponentModel.DataAnnotations;

namespace Store.DAL.Entities
{
    public class SalesmanEntity
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        public byte Experience { get; set; }

        public uint Salary { get; set; }
    }
}
