﻿using System.ComponentModel.DataAnnotations;

namespace Store.DAL.Entities
{
    public class MerchandiseEntity
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        public virtual SizeEntity Size { get; set; }
        public virtual CategoryEntity Category { get; set; }
        public virtual ColorEntity Color { get; set; }

        public uint StockQuantity { get; set; }
    }
}
