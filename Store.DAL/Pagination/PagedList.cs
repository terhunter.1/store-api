﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Pagination
{
    public class PagedList<T> : List<T>
    {
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }

        public PagedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            CurrentPage = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            this.AddRange(items);
        }

        public bool HasPreviousPage
        {
            get
            {
                return (CurrentPage > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (CurrentPage < TotalPages);
            }
        }

        public static async Task<PagedList<T>> GetPagedListAsync(IQueryable<T> source, int pageIndex, int pageSize, CancellationToken ct)
        {
            var count = await source.CountAsync(ct);
            var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync(ct);
            return new PagedList<T>(items, count, pageIndex, pageSize);
        }
    }
}
