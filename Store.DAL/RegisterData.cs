﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using Store.DAL.Repositories;

namespace Store.DAL
{
    public static class RegisterData
    {
        public static void AddDataAccess(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddScoped<IGenericRepository<DepartmentEntity>, DepartmentRepository>();
            serviceCollection.AddScoped<IGenericRepository<CategoryEntity>, CategoryRepository>();
            serviceCollection.AddScoped<IGenericRepository<ColorEntity>, ColorRepository>();
            serviceCollection.AddScoped<IGenericRepository<MerchandiseEntity>, MerchandiseRepository>();
            serviceCollection.AddScoped<IGenericRepository<SalesmanEntity>, SalesmanRepository>();
            serviceCollection.AddScoped<IGenericRepository<SalesEntity>, SalesRepository>();
            serviceCollection.AddScoped<IGenericRepository<SizeEntity>, SizeRepository>();
            serviceCollection.AddScoped<IGenericRepository<UserEntity>, UserRepository>();

            serviceCollection.AddDbContext<DatabaseContext>(context =>
                context
                    .UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
            );
        }
    }
}
