﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class MerchandiseRepository : GenericRepository<MerchandiseEntity>
    {
        public MerchandiseRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(MerchandiseEntity merchandise, CancellationToken ct)
        {
            MerchandiseEntity entity = await _dbSet.FindAsync(new object[] { merchandise.Id }, ct);
            if (entity == null) throw new StoreException(ErrorCodes.IdNotFound);

            SizeEntity size = await _context.Sizes.FirstOrDefaultAsync((i => i.Id == merchandise.Size.Id), ct);
            if (size == null) throw new StoreException(ErrorCodes.SizeNotFound);

            CategoryEntity category = await _context.Categories.FirstOrDefaultAsync((i => i.Id == merchandise.Category.Id), ct);
            if (category == null) throw new StoreException(ErrorCodes.CategoryNotFound);

            ColorEntity color = await _context.Colors.FirstOrDefaultAsync((i => i.Id == merchandise.Color.Id), ct);
            if (color == null) throw new StoreException(ErrorCodes.ColorNotFound);

            EntityEntry<MerchandiseEntity> ctx = _context.Entry(entity);

            object newMerchandise = new
            {
                merchandise.Id,
                merchandise.Name,
                merchandise.StockQuantity,
                SizeId = size.Id,
                CategoryId = category.Id,
                ColorId = color.Id,
            };

            ctx.CurrentValues.SetValues(newMerchandise);

            await _context.SaveChangesAsync(ct);
            return true;
        }
        public override async Task<MerchandiseEntity> AddAsync(MerchandiseEntity merchandise, CancellationToken ct)
        {
            SizeEntity size = await _context.Sizes.FirstOrDefaultAsync((i => i.Id == merchandise.Size.Id), ct);
            if (size == null) throw new StoreException(ErrorCodes.SizeNotFound);

            CategoryEntity category = await _context.Categories.FirstOrDefaultAsync((i => i.Id == merchandise.Category.Id), ct);
            if (category == null) throw new StoreException(ErrorCodes.CategoryNotFound);

            ColorEntity color = await _context.Colors.FirstOrDefaultAsync((i => i.Id == merchandise.Color.Id), ct);
            if (color == null) throw new StoreException(ErrorCodes.ColorNotFound);

            MerchandiseEntity newMerchandise = new()
            {
                Id = merchandise.Id,
                Name = merchandise.Name,
                StockQuantity = merchandise.StockQuantity,
                Size = size,
                Category = category,
                Color = color,
            };

            _dbSet.Add(newMerchandise);
            await _context.SaveChangesAsync(ct);
            return newMerchandise;
        }
    }
}