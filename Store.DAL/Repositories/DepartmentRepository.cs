﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class DepartmentRepository : GenericRepository<DepartmentEntity>
    {
        public DepartmentRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(DepartmentEntity department, CancellationToken ct)
        {
            DepartmentEntity entity = await _dbSet.FindAsync(new object[] { department.Id }, ct);
            if (entity == null) { throw new StoreException(ErrorCodes.IdNotFound); }
            EntityEntry<DepartmentEntity> ctx = _context.Entry(entity);
            ctx.CurrentValues.SetValues(department);
            await _context.SaveChangesAsync(ct);
            return true;

        }
        public override async Task<DepartmentEntity> AddAsync(DepartmentEntity department, CancellationToken ct)
        {
            _dbSet.Add(department);
            await _context.SaveChangesAsync(ct);
            return department;
        }
    }
}