﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class UserRepository : GenericRepository<UserEntity>
    {
        public UserRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(UserEntity user, CancellationToken ct)
        {
            UserEntity entity = await _dbSet.FindAsync(new object[] { user.Id }, ct);
            if (entity == null) { throw new StoreException(ErrorCodes.IdNotFound); }
            EntityEntry<UserEntity> ctx = _context.Entry(entity);
            ctx.CurrentValues.SetValues(user);
            await _context.SaveChangesAsync(ct);
            return true;
        }
        public override async Task<UserEntity> AddAsync(UserEntity user, CancellationToken ct)
        {
            _dbSet.Add(user);
            await _context.SaveChangesAsync(ct);
            return user;
        }
    }
}