﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class SizeRepository : GenericRepository<SizeEntity>
    {
        public SizeRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(SizeEntity size, CancellationToken ct)
        {
            SizeEntity entity = await _dbSet.FindAsync(new object[] { size.Id }, ct);
            if (entity == null) { throw new StoreException(ErrorCodes.IdNotFound); }
            EntityEntry<SizeEntity> ctx = _context.Entry(entity);
            ctx.CurrentValues.SetValues(size);
            await _context.SaveChangesAsync(ct);
            return true;
        }
        public override async Task<SizeEntity> AddAsync(SizeEntity size, CancellationToken ct)
        {
            _dbSet.Add(size);
            await _context.SaveChangesAsync(ct);
            return size;
        }
    }
}