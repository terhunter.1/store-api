﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class SalesRepository : GenericRepository<SalesEntity>
    {
        public SalesRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(SalesEntity sale, CancellationToken ct)
        {
            SalesEntity entity = await _dbSet.FindAsync(new object[] { sale.Id }, ct);
            if (entity == null) throw new StoreException(ErrorCodes.IdNotFound);

            MerchandiseEntity merchandise = await _context.Merchandises.FirstOrDefaultAsync((i => i.Id == sale.Merchandise.Id), ct);
            if (merchandise == null) throw new StoreException(ErrorCodes.MerchandiseNotFound);
            
            SalesmanEntity salesman = await _context.Salesmen.FirstOrDefaultAsync((i => i.Id == sale.Salesman.Id), ct);
            if (salesman == null) throw new StoreException(ErrorCodes.SalesmanNotFound);

            EntityEntry<SalesEntity> ctx = _context.Entry(entity);

            object newSale = new
            {
                sale.Id,
                sale.Quantity,
                MerchandiseId = merchandise.Id,
                SalesmanId = salesman.Id
            };

            ctx.CurrentValues.SetValues(newSale);

            await _context.SaveChangesAsync(ct);
            return true;

        }
        public override async Task<SalesEntity> AddAsync(SalesEntity sale, CancellationToken ct)
        {
            MerchandiseEntity merchandise = await _context.Merchandises.FirstOrDefaultAsync((i => i.Id == sale.Merchandise.Id), ct);
            if (merchandise == null) throw new StoreException(ErrorCodes.MerchandiseNotFound);

            SalesmanEntity salesman = await _context.Salesmen.FirstOrDefaultAsync((i => i.Id == sale.Salesman.Id), ct);
            if (salesman == null) throw new StoreException(ErrorCodes.SalesmanNotFound);

            SalesEntity newSale = new()
            {
                Id = sale.Id,
                Quantity = sale.Quantity,
                Merchandise = merchandise,
                Salesman = salesman,
            };

            _dbSet.Add(newSale);
            await _context.SaveChangesAsync(ct);
            return newSale;
        }
    }
}