﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class SalesmanRepository : GenericRepository<SalesmanEntity>
    {
        public SalesmanRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(SalesmanEntity salesman, CancellationToken ct)
        {
            SalesmanEntity entity = await _dbSet.FindAsync(new object[] { salesman.Id }, ct);
            if (entity == null) throw new StoreException(ErrorCodes.IdNotFound);

            EntityEntry<SalesmanEntity> ctx = _context.Entry(entity);
            ctx.CurrentValues.SetValues(salesman);
            await _context.SaveChangesAsync(ct);
            return true;

        }
        public override async Task<SalesmanEntity> AddAsync(SalesmanEntity salesman, CancellationToken ct)
        {
            _dbSet.Add(salesman);
            await _context.SaveChangesAsync(ct);
            return salesman;
        }
    }
}