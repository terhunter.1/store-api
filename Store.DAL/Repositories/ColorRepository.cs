﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class ColorRepository : GenericRepository<ColorEntity>
    {
        public ColorRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(ColorEntity color, CancellationToken ct)
        {
            ColorEntity entity = await _dbSet.FindAsync(new object[] { color.Id }, ct);
            if (entity == null) { throw new StoreException(ErrorCodes.IdNotFound); }
            EntityEntry<ColorEntity> ctx = _context.Entry(entity);
            ctx.CurrentValues.SetValues(color);
            await _context.SaveChangesAsync(ct);
            return true;
        }
        public override async Task<ColorEntity> AddAsync(ColorEntity color, CancellationToken ct)
        {
            _dbSet.Add(color);
            await _context.SaveChangesAsync(ct);
            return color;
        }
    }
}