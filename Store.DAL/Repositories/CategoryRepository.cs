﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Store.API.Codes;
using Store.DAL.Contexts;
using Store.DAL.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    class CategoryRepository : GenericRepository<CategoryEntity>
    {
        public CategoryRepository(DatabaseContext context) : base(context)
        {
        }
        public override async Task<bool> UpdateAsync(CategoryEntity category, CancellationToken ct)
        {
            CategoryEntity entity = await _dbSet.FindAsync(new object[] { category.Id }, ct);
            if (entity == null) throw new StoreException(ErrorCodes.IdNotFound);

            DepartmentEntity department = await _context.Departments.FirstOrDefaultAsync((i => i.Id == category.Department.Id), ct);
            if (department == null) throw new StoreException(ErrorCodes.DepartmentNotFound);
            
            EntityEntry<CategoryEntity> ctx = _context.Entry(entity);

            object newCategory = new
            {
                category.Id,
                category.Name,
                DepartmentId = department.Id,
            };

            ctx.CurrentValues.SetValues(newCategory);

            await _context.SaveChangesAsync(ct);
            return true;

        }
        public override async Task<CategoryEntity> AddAsync(CategoryEntity category, CancellationToken ct)
        {
            DepartmentEntity department = await _context.Departments.FirstOrDefaultAsync((i => i.Id == category.Department.Id), ct);
            if (department == null) throw new StoreException(ErrorCodes.DepartmentNotFound);

            CategoryEntity newCategory = new()
            {
                Id = category.Id,
                Name = category.Name,
                Department = department,
            };

            _dbSet.Add(newCategory);
            await _context.SaveChangesAsync(ct);
            return newCategory;
        }
    }
}