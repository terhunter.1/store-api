﻿using Microsoft.EntityFrameworkCore;
using Store.DAL.Contexts;
using Store.DAL.Interfaces.Repositories;
using Store.DAL.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Repositories
{
    abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DatabaseContext _context;
        protected readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DatabaseContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public virtual async Task<PagedList<TEntity>> GetAsync(int? pageIndex, int? pageSize, CancellationToken ct)
        {
            return await PagedList<TEntity>.GetPagedListAsync(_dbSet.AsQueryable(), pageIndex ?? 1, pageSize ?? 20, ct);
        }
        public virtual async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression, CancellationToken ct)
        {
            return await _dbSet.Where(expression).ToListAsync(ct);
        }
        public virtual async Task<TEntity> GetByIdAsync(int id, CancellationToken ct)
        {
            return await _dbSet.FindAsync(new object[] { id }, ct);
        }
        public async Task DeleteAsync(int id, CancellationToken ct)
        {
            TEntity entity = await _dbSet.FindAsync(new object[] { id }, ct);
            if (entity != null)
            {
                _dbSet.Remove(entity);
                await _context.SaveChangesAsync(ct);
            }
        }
        public abstract Task<bool> UpdateAsync(TEntity entity, CancellationToken ct);
        public abstract Task<TEntity> AddAsync(TEntity entity, CancellationToken ct);
    }
}

