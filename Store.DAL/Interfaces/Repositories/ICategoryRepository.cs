﻿using Store.DAL.Entities;

namespace Store.DAL.Interfaces.Repositories
{
    interface ICategoryRepository : IGenericRepository<CategoryEntity>
    {
    }
}
