﻿using Store.DAL.Entities;

namespace Store.DAL.Interfaces.Repositories
{
    interface IColorRepository : IGenericRepository<ColorEntity>
    {
    }
}
