﻿using Store.DAL.Pagination;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Store.DAL.Interfaces.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<PagedList<TEntity>> GetAsync(int? pageIndex, int? pageSize, CancellationToken ct);
        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression, CancellationToken ct);
        Task<TEntity> GetByIdAsync(int id, CancellationToken ct);
        Task DeleteAsync(int id, CancellationToken ct);
        Task<bool> UpdateAsync(TEntity entity, CancellationToken ct);
        Task<TEntity> AddAsync(TEntity entity, CancellationToken ct);
    }
}
