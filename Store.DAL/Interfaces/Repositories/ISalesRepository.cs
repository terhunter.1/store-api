﻿using Store.DAL.Entities;

namespace Store.DAL.Interfaces.Repositories
{
    interface ISalesRepository : IGenericRepository<SalesEntity>
    {
    }
}
