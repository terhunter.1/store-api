﻿using Store.DAL.Entities;

namespace Store.DAL.Interfaces.Repositories
{
    interface IDepartmentRepository : IGenericRepository<DepartmentEntity>
    {
    }
}
