﻿using Store.DAL.Entities;

namespace Store.DAL.Interfaces.Repositories
{
    interface IMerchandiseRepository : IGenericRepository<MerchandiseEntity>
    {
    }
}
