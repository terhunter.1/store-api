﻿using Store.DAL.Entities;

namespace Store.DAL.Interfaces.Repositories
{
    interface ISizeRepository : IGenericRepository<SizeEntity>
    {
    }
}
