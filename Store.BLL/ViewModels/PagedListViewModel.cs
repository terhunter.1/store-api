﻿using System.Collections.Generic;

namespace Store.BLL.ViewModels
{
    public class PagedListViewModel<TModel>
    {
        public int TotalPage { get; set; }
        public int CurrentPage { get; set; }
        public IEnumerable<TModel> Data { get; set; }

        public PagedListViewModel(int totalPage, int currentPage, IEnumerable<TModel> list)
        {
            TotalPage = totalPage;
            CurrentPage = currentPage;
            Data = list;
        }
    }
}
