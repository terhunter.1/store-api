﻿namespace Store.BLL.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public DepartmentViewModel Department { get; set; }
        public string Name { get; set; }
    }
}
