﻿namespace Store.BLL.ViewModels
{
    public class SalesViewModel
    {
        public int Id { get; set; }
        public SalesmanViewModel Salesman { get; set; }
        public MerchandiseViewModel Merchandise { get; set; }
        public uint Quantity { get; set; }
    }
}
