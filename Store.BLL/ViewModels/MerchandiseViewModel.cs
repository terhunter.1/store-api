﻿namespace Store.BLL.ViewModels
{
    public class MerchandiseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SizeViewModel Size { get; set; }
        public ColorViewModel Color { get; set; }
        public CategoryViewModel Category { get; set; }
        public uint StockQuantity { get; set; }
    }
}
