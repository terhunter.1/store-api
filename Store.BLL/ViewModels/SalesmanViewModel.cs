﻿namespace Store.BLL.ViewModels
{
    public class SalesmanViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte Experience { get; set; }

        public uint Salary { get; set; }
    }
}
