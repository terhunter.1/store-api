﻿namespace Store.BLL.ViewModels
{
    public class ColorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
