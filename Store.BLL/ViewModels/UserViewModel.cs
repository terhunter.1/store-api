﻿namespace Store.BLL.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Permission { get; set; }
    }
}
