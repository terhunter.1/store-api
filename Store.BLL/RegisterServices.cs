﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Store.BLL.Interfaces;
using Store.BLL.Services;
using Store.BLL.ViewModels;
using Store.DAL;

namespace Store.BLL
{
    public static class RegisterServices
    {
        public static void AddBusinessServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddScoped<IGenericService<DepartmentViewModel>, DepartmentService>();
            serviceCollection.AddScoped<IGenericService<ColorViewModel>, ColorService>();
            serviceCollection.AddScoped<IGenericService<SizeViewModel>, SizeService>();
            serviceCollection.AddScoped<IGenericService<CategoryViewModel>, CategoryService>();
            serviceCollection.AddScoped<IGenericService<SalesmanViewModel>, SalesmanService>();
            serviceCollection.AddScoped<IMerchandiseService, MerchandiseService>();
            serviceCollection.AddScoped<IGenericService<SalesViewModel>, SalesService>();
            serviceCollection.AddScoped<ISaleOperationService, SaleOperationService>();
            serviceCollection.AddScoped<IUserService, UserService>();

            serviceCollection.AddDataAccess(configuration);
        }
    }
}
