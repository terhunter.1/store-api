﻿using Store.BLL.ViewModels;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Interfaces
{
    public interface IUserService : IGenericService<UserViewModel>
    {
        Task<bool> UpdateUserPermission(int id, string permission, CancellationToken ct);
        Task<object> Authorize(string login, string password, CancellationToken ct);
    }
}
