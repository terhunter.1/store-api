﻿using System.Threading;
using System.Threading.Tasks;
using Store.BLL.ViewModels;

namespace Store.BLL.Interfaces
{
    public interface ISaleOperationService
    {
        Task<SalesViewModel> AddSale(SalesViewModel newSales, CancellationToken ct);
    }
}
