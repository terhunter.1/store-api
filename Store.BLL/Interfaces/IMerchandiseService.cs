﻿using Store.BLL.Models.Filters;
using Store.BLL.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Interfaces
{
    public interface IMerchandiseService : IGenericService<MerchandiseViewModel>
    {
        Task<IEnumerable<MerchandiseViewModel>> GetWithFilters(MerchandiseFilterModel filters, CancellationToken ct);
    }
}
