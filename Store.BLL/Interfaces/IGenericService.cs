﻿using Store.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Interfaces
{
    public interface IGenericService<TModel>
    {
        Task<PagedListViewModel<TModel>> GetAsync(int? pageIndex, int? pageSize, CancellationToken ct);
        Task<IEnumerable<TModel>> GetAsync(Expression<Func<TModel, bool>> expression, CancellationToken ct);
        Task<TModel> GetByIdAsync(int id, CancellationToken ct);
        Task DeleteAsync(int id, CancellationToken ct);
        Task<bool> UpdateAsync(int id, TModel model, CancellationToken ct);
        Task<TModel> AddAsync(TModel model, CancellationToken ct);

    }
}
