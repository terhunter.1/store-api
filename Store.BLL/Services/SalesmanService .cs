﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class SalesmanService : GenericService<SalesmanViewModel, SalesmanEntity>
    {
        public SalesmanService(IGenericRepository<SalesmanEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, SalesmanViewModel model, CancellationToken ct)
        {
            try
            {
                SalesmanEntity updateValue = new () { Id = Id, Name = model.Name };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }

        }
        public override async Task<SalesmanViewModel> AddAsync(SalesmanViewModel model, CancellationToken ct)
        {
            return _mapper.Map<SalesmanViewModel>(
                await _repository.AddAsync(_mapper.Map<SalesmanEntity>(model), ct));
        }

    }
}
