﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class ColorService : GenericService<ColorViewModel, ColorEntity>
    {
        public ColorService(IGenericRepository<ColorEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, ColorViewModel model, CancellationToken ct)
        {
            try
            {
                ColorEntity updateValue = new() { Id = Id, Name = model.Name };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }

        }
        public override async Task<ColorViewModel> AddAsync(ColorViewModel model, CancellationToken ct)
        {
            return _mapper.Map<ColorViewModel>(
                await _repository.AddAsync(_mapper.Map<ColorEntity>(model), ct));
        }

    }
}
