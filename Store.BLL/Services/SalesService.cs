﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class SalesService : GenericService<SalesViewModel, SalesEntity>
    {
        public SalesService(IGenericRepository<SalesEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, SalesViewModel model, CancellationToken ct)
        {
            try
            {
                SalesEntity updateValue = new()
                {
                    Id = Id,
                    Quantity = model.Quantity,
                    Merchandise = new MerchandiseEntity() { Id = model.Merchandise.Id },
                    Salesman = new SalesmanEntity() { Id = model.Salesman.Id },
                };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }
        }
        public override async Task<SalesViewModel> AddAsync(SalesViewModel model, CancellationToken ct)
        {
            try
            {
                return _mapper.Map<SalesViewModel>(
                    await _repository.AddAsync(_mapper.Map<SalesEntity>(model), ct));
            }
            catch (StoreException)
            {
                throw;
            }
        }
    }
}
