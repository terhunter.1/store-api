﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.Interfaces;
using Store.BLL.Models.Filters;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class MerchandiseService : GenericService<MerchandiseViewModel, MerchandiseEntity>, IMerchandiseService
    {
        public MerchandiseService(IGenericRepository<MerchandiseEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, MerchandiseViewModel model, CancellationToken ct)
        {
            try
            {
                MerchandiseEntity updateValue = new()
                {
                    Id = Id,
                    Name = model.Name,
                    StockQuantity = model.StockQuantity,
                    Size = new SizeEntity() { Id = model.Size.Id },
                    Color = new ColorEntity() { Id = model.Color.Id },
                    Category = new CategoryEntity() { Id = model.Category.Id }
                };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }

        }
        public override async Task<MerchandiseViewModel> AddAsync(MerchandiseViewModel model, CancellationToken ct)
        {
            try
            {
                return _mapper.Map<MerchandiseViewModel>(
                    await _repository.AddAsync(_mapper.Map<MerchandiseEntity>(model), ct));
            }
            catch (StoreException)
            {
                throw;
            }
        }
        public async Task<IEnumerable<MerchandiseViewModel>> GetWithFilters(MerchandiseFilterModel filters, CancellationToken ct)
        {
            Expression<Func<MerchandiseEntity, bool>> expr = x =>
            (filters.ColorId == null || filters.ColorId == x.Color.Id) &&
            (filters.CategoryId == null || filters.CategoryId == x.Category.Id) && 
            (filters.SizeId == null || filters.SizeId == x.Size.Id) && 
            (filters.DepartmentId == null || filters.DepartmentId == x.Category.Department.Id);

            return _mapper.Map<IEnumerable<MerchandiseViewModel>>(await _repository.GetAsync(expr, ct));
        }

    }
}
