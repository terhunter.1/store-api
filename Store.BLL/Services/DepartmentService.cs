﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class DepartmentService : GenericService<DepartmentViewModel, DepartmentEntity>
    {
        public DepartmentService(IGenericRepository<DepartmentEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, DepartmentViewModel model, CancellationToken ct)
        {
            try
            {
                DepartmentEntity updateValue = new () { Id = Id, Name = model.Name };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }
        }
        public override async Task<DepartmentViewModel> AddAsync(DepartmentViewModel model, CancellationToken ct)
        {
            return _mapper.Map<DepartmentViewModel>(
                await _repository.AddAsync(_mapper.Map<DepartmentEntity>(model), ct));
        }

    }
}
