﻿using AutoMapper;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using Store.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public abstract class GenericService<TModel, TEntity> : IGenericService<TModel>
        where TEntity : class
        where TModel : class
    {
        protected readonly IGenericRepository<TEntity> _repository;
        protected readonly IMapper _mapper;

        public GenericService(IGenericRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public virtual async Task<PagedListViewModel<TModel>> GetAsync(int? pageIndex, int? pageSize, CancellationToken ct)
        {
            var result = await _repository.GetAsync(pageIndex, pageSize, ct);
            return new PagedListViewModel<TModel>(result.TotalPages, result.CurrentPage, _mapper.Map<IEnumerable<TModel>>(result));
        }
        public virtual async Task<IEnumerable<TModel>> GetAsync(Expression<Func<TModel, bool>> expression, CancellationToken ct)
        {
            return _mapper.Map<IEnumerable<TModel>>(await _repository.GetAsync(
                _mapper.Map<Expression<Func<TEntity, bool>>>(expression), ct)
               );
        }
        public virtual async Task<TModel> GetByIdAsync(int id, CancellationToken ct)
        {
            return _mapper.Map<TModel>(await _repository.GetByIdAsync(id, ct));
        }
        public async Task DeleteAsync(int id, CancellationToken ct)
        {
            await _repository.DeleteAsync(id, ct);
        }
        public abstract Task<bool> UpdateAsync(int id, TModel model, CancellationToken ct);
        public abstract Task<TModel> AddAsync(TModel model, CancellationToken ct);

    }
}
