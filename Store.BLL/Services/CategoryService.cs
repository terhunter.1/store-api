﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class CategoryService : GenericService<CategoryViewModel, CategoryEntity>
    {
        public CategoryService(IGenericRepository<CategoryEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, CategoryViewModel model, CancellationToken ct)
        {
            try
            {
                CategoryEntity updateValue = new() { Id = Id, Name = model.Name, Department = new DepartmentEntity() { Id = model.Department.Id } };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }

        }
        public override async Task<CategoryViewModel> AddAsync(CategoryViewModel model, CancellationToken ct)
        {
            try
            {
                return _mapper.Map<CategoryViewModel>(
                    await _repository.AddAsync(_mapper.Map<CategoryEntity>(model), ct));
            }
            catch (StoreException)
            {
                throw;
            }
        }

        public override async Task<CategoryViewModel> GetByIdAsync(int id, CancellationToken ct)
        {
            return _mapper.Map<CategoryViewModel>(await _repository.GetByIdAsync(id, ct));
        }
    }
}
