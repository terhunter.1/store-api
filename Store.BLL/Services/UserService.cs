﻿using AutoMapper;
using Store.API.Codes;
using Store.Authorization;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class UserService : GenericService<UserViewModel, UserEntity>, IUserService
    {
        public UserService(IGenericRepository<UserEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, UserViewModel model, CancellationToken ct)
        {
            try
            {
                UserEntity updateValue = new() { Id = Id, Login = model.Login, Permission = model.Permission };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }

        }
        public override async Task<UserViewModel> AddAsync(UserViewModel model, CancellationToken ct)
        {
            return _mapper.Map<UserViewModel>(
                await _repository.AddAsync(_mapper.Map<UserEntity>(model), ct));
        }

        public async Task<bool> UpdateUserPermission(int id, string permission, CancellationToken ct)
        {
            try
            {
                UserEntity updateEntity = await _repository.GetByIdAsync(id, ct);
                updateEntity.Permission = permission;
                return await _repository.UpdateAsync(updateEntity, ct);
            }
            catch (StoreException)
            {
                throw;
            }
        }

        public async Task<object> Authorize(string login, string password, CancellationToken ct)
        {
            try
            {
                IEnumerable<UserEntity> users = await _repository.GetAsync(x => x.Login == login && x.Password == password, ct);
                if (users.Count() != 1) { throw new StoreException(ErrorCodes.Unauthorized); }
                UserEntity user = users.First();
                string token = TokenGenerator.CreateToken(user);
                return new { access_token = token, login = user.Login };
            }
            catch (StoreException)
            {
                throw;
            }
        }

    }
}
