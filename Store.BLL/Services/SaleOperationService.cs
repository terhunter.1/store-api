﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class SaleOperationService : ISaleOperationService
    {
        protected readonly IGenericRepository<MerchandiseEntity> MerchandiseRep;
        protected readonly IGenericRepository<SalesEntity> SalesRep;
        protected readonly IMapper _mapper;

        public SaleOperationService(IGenericRepository<MerchandiseEntity> merchRepository, IGenericRepository<SalesEntity> salesRepository, IMapper mapper)
        {
            MerchandiseRep = merchRepository;
            SalesRep = salesRepository;
            _mapper = mapper;
        }

        public async Task<SalesViewModel> AddSale(SalesViewModel model, CancellationToken ct)
        {
            try
            {
                MerchandiseEntity merch = await MerchandiseRep.GetByIdAsync(model.Merchandise.Id, ct);
                if (merch == null) throw new StoreException(ErrorCodes.MerchandiseNotFound);

                if (merch.StockQuantity < model.Quantity) throw new StoreException(ErrorCodes.NotEnoughQuantity);

                merch.StockQuantity -= model.Quantity;

                await MerchandiseRep.UpdateAsync(merch, ct);

                return _mapper.Map<SalesViewModel>(
                    await SalesRep.AddAsync(_mapper.Map<SalesEntity>(model), ct));
            }
            catch (StoreException)
            {
                throw;
            }
        }

    }
}
