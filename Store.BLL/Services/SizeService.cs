﻿using AutoMapper;
using Store.API.Codes;
using Store.BLL.ViewModels;
using Store.DAL.Entities;
using Store.DAL.Interfaces.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace Store.BLL.Services
{
    public class SizeService : GenericService<SizeViewModel, SizeEntity>
    {
        public SizeService(IGenericRepository<SizeEntity> repository, IMapper mapper) : base(repository, mapper) { }

        public override async Task<bool> UpdateAsync(int Id, SizeViewModel model, CancellationToken ct)
        {
            try
            {
                SizeEntity updateValue = new () { Id = Id, Name = model.Name };
                return await _repository.UpdateAsync(updateValue, ct);
            }
            catch (StoreException)
            {
                throw;
            }

        }
        public override async Task<SizeViewModel> AddAsync(SizeViewModel model, CancellationToken ct)
        {
            return _mapper.Map<SizeViewModel>(
                await _repository.AddAsync(_mapper.Map<SizeEntity>(model), ct));
        }

    }
}
