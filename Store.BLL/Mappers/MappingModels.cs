﻿using AutoMapper;
using Store.BLL.ViewModels;
using Store.DAL.Entities;

namespace Store.BLL.Mappers
{
    public class MappingModels : Profile
    {
        public MappingModels()
        {
            CreateMap<DepartmentViewModel, DepartmentEntity>().ReverseMap();
            CreateMap<ColorViewModel, ColorEntity>().ReverseMap();
            CreateMap<SizeViewModel, SizeEntity>().ReverseMap();
            CreateMap<CategoryViewModel, CategoryEntity>().ReverseMap();
            CreateMap<SalesmanViewModel, SalesmanEntity>().ReverseMap();
            CreateMap<MerchandiseViewModel, MerchandiseEntity>().ReverseMap();
            CreateMap<SalesViewModel, SalesEntity>().ReverseMap();
            CreateMap<UserViewModel, UserEntity>().ReverseMap();
        }
    }
}
