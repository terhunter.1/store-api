﻿namespace Store.BLL.Models.Filters
{
    public class MerchandiseFilterModel
    {
        public int? CategoryId { get; set; }
        public int? ColorId { get; set; }
        public int? SizeId { get; set; }
        public int? DepartmentId { get; set; }
    }
}
