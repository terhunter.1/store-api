﻿using Microsoft.IdentityModel.Tokens;
using Store.DAL.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Store.Authorization
{
    public static class TokenGenerator
    {
        public static string CreateToken(UserEntity user)
        {
            ClaimsIdentity identity = GetIdentity(user);
            DateTime now = DateTime.UtcNow;

            JwtSecurityToken jwt = new (
                    issuer: TokenData.ISSUER,
                    audience: TokenData.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(TokenData.LIFETIME)),
                    signingCredentials: new SigningCredentials(TokenData.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        public static ClaimsIdentity GetIdentity(UserEntity user)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Permission)
                };
            ClaimsIdentity claimsIdentity =
            new(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
