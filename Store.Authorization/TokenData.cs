﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Store.Authorization
{
    public class TokenData
    {
        public const string ISSUER = "Store.Api";
        public const string AUDIENCE = "Store.Client";
        const string KEY = "my_secretkey!123";
        public const int LIFETIME = 3600;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
