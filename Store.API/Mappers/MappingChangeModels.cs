﻿using AutoMapper;
using Store.API.Models;
using Store.BLL.ViewModels;

namespace Store.API.Mappers
{
    public class MappingChangeModels : Profile
    {
        public MappingChangeModels()
        {
            CreateMap<DepartmentViewModel, ChangeDepartmentModel>().ReverseMap();
            CreateMap<ColorViewModel, ChangeColorModel>().ReverseMap();
            CreateMap<SizeViewModel, ChangeSizeModel>().ReverseMap();
            CreateMap<SalesmanViewModel, ChangeSalesmanModel>().ReverseMap();

            CreateMap<CategoryViewModel, ChangeCategoryModel>()
                .ForMember(
                    dest => dest.DepartmentId,
                    opt => opt.MapFrom(x => x.Department.Id)
                )
                .ReverseMap();
            
            
            CreateMap<MerchandiseViewModel, ChangeMerchandiseModel>()
                .ForMember(
                    dest => dest.SizeId,
                    opt => opt.MapFrom(x => x.Size.Id)
                )
                .ForMember(
                    dest => dest.ColorId,
                    opt => opt.MapFrom(x => x.Color.Id)
                )
                .ForMember(
                    dest => dest.CategoryId,
                    opt => opt.MapFrom(x => x.Category.Id)
                )
                .ReverseMap();

            CreateMap<SalesViewModel, ChangeSaleModel>()
                .ForMember(
                    dest => dest.MerchandiseId,
                    opt => opt.MapFrom(x => x.Merchandise.Id)
                )
                .ForMember(
                    dest => dest.SalesmanId,
                    opt => opt.MapFrom(x => x.Salesman.Id)
                )
                .ReverseMap();
        }
    }
}
