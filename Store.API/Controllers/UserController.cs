﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Store.API.Codes;
using Store.API.Models;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using System.Threading;
using System.Threading.Tasks;

namespace Store.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        protected readonly IUserService _service;
        public UserController(IUserService service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<PagedListViewModel<UserViewModel>> Get([FromQuery] int? pageIndex, [FromQuery] int? pageSize, CancellationToken ct)
        {
            return await _service.GetAsync(pageIndex, pageSize, ct);
        }

        [HttpGet("{id}")]
        public async Task<UserViewModel> Get(int id, CancellationToken ct)
        {
            return await _service.GetByIdAsync(id, ct);
        }

        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] AuthorizationModel userData, CancellationToken ct)
        {
            try
            {
                return Ok(await _service.Authorize(userData.Login, userData.Password, ct));
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }

        //[HttpPut]
        //public async Task<IActionResult> Put([FromQuery] int id, [FromBody] ChangeSizeModel size, CancellationToken ct)
        //{
        //    try
        //    {
        //        if (await _service.UpdateAsync(id, _mapper.Map<SizeViewModel>(size), ct))
        //        {
        //            return Ok();
        //        }
        //        else
        //        {
        //            return BadRequest(new { message = "Update is unpossible" });
        //        }
        //    }
        //    catch (StoreException ex)
        //    {
        //        return BadRequest(ex.GetError());
        //    }
        //}

        //[HttpDelete("{id}")]
        //public async Task Delete(int id, CancellationToken ct)
        //{
        //    await _service.DeleteAsync(id, ct);
        //}
    }
}
