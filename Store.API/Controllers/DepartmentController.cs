﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Store.API.Codes;
using Store.API.Models;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.API.Controllers
{
    [Route("api/[controller]")]
    public class DepartmentController : BaseController
    {
        protected readonly IGenericService<DepartmentViewModel> _service;
        public DepartmentController(IGenericService<DepartmentViewModel> service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<PagedListViewModel<DepartmentViewModel>> Get([FromQuery] int? pageIndex, [FromQuery] int? pageSize, CancellationToken ct)
        {
            return await _service.GetAsync(pageIndex, pageSize, ct);
        }

        [HttpGet("{id}")]
        public async Task<DepartmentViewModel> Get(int id, CancellationToken ct)
        {
            return _mapper.Map<DepartmentViewModel>(await _service.GetByIdAsync(id, ct));
        }

        [HttpPost]
        public async Task<DepartmentViewModel> Post([FromBody] ChangeDepartmentModel department, CancellationToken ct)
        {
            return _mapper.Map<DepartmentViewModel>(await _service.AddAsync(_mapper.Map<DepartmentViewModel>(department), ct));
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromQuery] int id, [FromBody] ChangeDepartmentModel department, CancellationToken ct)
        {
            try
            {
                if (await _service.UpdateAsync(id, _mapper.Map<DepartmentViewModel>(department), ct))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(new { message = "Update is unpossible" });
                }
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id, CancellationToken ct)
        {
            await _service.DeleteAsync(id, ct);
        }
    }
}
