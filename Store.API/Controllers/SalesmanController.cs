﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Store.API.Codes;
using Store.API.Models;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.API.Controllers
{
    [Route("api/[controller]")]
    public class SalesmanController : BaseController
    {
        protected readonly IGenericService<SalesmanViewModel> _service;
        public SalesmanController(IGenericService<SalesmanViewModel> service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<PagedListViewModel<SalesmanViewModel>> Get([FromQuery] int? pageIndex, [FromQuery] int? pageSize, CancellationToken ct)
        {
            return await _service.GetAsync(pageIndex, pageSize, ct);
        }

        [HttpGet("{id}")]
        public async Task<SalesmanViewModel> Get(int id, CancellationToken ct)
        {
            return _mapper.Map<SalesmanViewModel>(await _service.GetByIdAsync(id, ct));
        }

        [HttpPost]
        public async Task<SalesmanViewModel> Post([FromBody] ChangeSalesmanModel salesman, CancellationToken ct)
        {
            return _mapper.Map<SalesmanViewModel>(await _service.AddAsync(_mapper.Map<SalesmanViewModel>(salesman), ct));
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromQuery] int id, [FromBody] ChangeSalesmanModel salesman, CancellationToken ct)
        {
            try
            {
                if (await _service.UpdateAsync(id, _mapper.Map<SalesmanViewModel>(salesman), ct))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(new { message = "Update is unpossible" });
                }
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id, CancellationToken ct)
        {
            await _service.DeleteAsync(id, ct);
        }
    }
}
