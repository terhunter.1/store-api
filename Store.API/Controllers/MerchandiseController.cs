﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Store.API.Codes;
using Store.API.Models;
using Store.BLL.Interfaces;
using Store.BLL.Models.Filters;
using Store.BLL.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.API.Controllers
{
    [Route("api/[controller]")]
    public class MerchandiseController : BaseController
    {
        protected readonly IMerchandiseService _service;
        public MerchandiseController(IMerchandiseService service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<PagedListViewModel<MerchandiseViewModel>> Get([FromQuery] int? pageIndex, [FromQuery] int? pageSize, CancellationToken ct)
        {
            return await _service.GetAsync(pageIndex, pageSize, ct);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> GetWithFilters([FromBody] MerchandiseFilterModel filters, CancellationToken ct)
        {
            try
            {
                return Ok(await _service.GetWithFilters(filters, ct));
            }
            catch (StoreException ex) {
                return BadRequest(ex.GetError());
            }
        }

        [HttpGet("{id}")]
        public async Task<MerchandiseViewModel> Get(int id, CancellationToken ct)
        {
            return await _service.GetByIdAsync(id, ct);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChangeMerchandiseModel merchandise, CancellationToken ct)
        {
            try
            {
                return Ok(_mapper.Map<MerchandiseViewModel>(await _service.AddAsync(_mapper.Map<MerchandiseViewModel>(merchandise), ct)));
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromQuery] int id, [FromBody] ChangeMerchandiseModel merchandise, CancellationToken ct)
        {
            try
            {
                if (await _service.UpdateAsync(id, _mapper.Map<MerchandiseViewModel>(merchandise), ct))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(new { message = "Update is unpossible" });
                }
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id, CancellationToken ct)
        {
            await _service.DeleteAsync(id, ct);
        }
    }
}
