﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Store.API.Codes;
using Store.API.Models;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Store.API.Controllers
{
    [Route("api/[controller]")]
    public class SalesController : BaseController
    {
        protected readonly IGenericService<SalesViewModel> _service;
        public SalesController(IGenericService<SalesViewModel> service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<PagedListViewModel<SalesViewModel>> Get([FromQuery] int? pageIndex, [FromQuery] int? pageSize, CancellationToken ct)
        {
            return await _service.GetAsync(pageIndex, pageSize, ct);
        }

        [HttpGet("{id}")]
        public async Task<SalesViewModel> Get(int id, CancellationToken ct)
        {
            return await _service.GetByIdAsync(id, ct);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChangeSaleModel sale, CancellationToken ct)
        {
            try
            {
                return Ok(_mapper.Map<SalesViewModel>(await _service.AddAsync(_mapper.Map<SalesViewModel>(sale), ct)));
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromQuery] int id, [FromBody] ChangeSaleModel sale, CancellationToken ct)
        {
            try
            {
                if (await _service.UpdateAsync(id, _mapper.Map<SalesViewModel>(sale), ct))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(new { message = "Update is unpossible" });
                }
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id, CancellationToken ct)
        {
            await _service.DeleteAsync(id, ct);
        }
    }
}
