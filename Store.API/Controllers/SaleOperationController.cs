﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Store.API.Codes;
using Store.API.Models;
using Store.BLL.Interfaces;
using Store.BLL.ViewModels;
using System.Threading;
using System.Threading.Tasks;

namespace Store.API.Controllers
{
    [Route("api/[controller]")]
    public class SaleOperationController : BaseController
    {
        protected readonly ISaleOperationService _service;
        public SaleOperationController(ISaleOperationService service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChangeSaleModel sale, CancellationToken ct)
        {
            try
            {
                var result = _mapper.Map<SalesViewModel>(await _service.AddSale(_mapper.Map<SalesViewModel>(sale), ct));

                return Ok(result);
            }
            catch (StoreException ex)
            {
                return BadRequest(ex.GetError());
            }
        }
    }
}
