﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class ChangeCategoryModelValidator: AbstractValidator<ChangeCategoryModel>
    {
        private const int maxLengthName = 20;
        private const int minId = 1;
        public ChangeCategoryModelValidator() {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(maxLengthName);
            RuleFor(x => x.DepartmentId).GreaterThanOrEqualTo(minId);
        }
    }
}
