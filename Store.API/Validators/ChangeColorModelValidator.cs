﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class ChangeColorModelValidator: AbstractValidator<ChangeColorModel>
    {
        private const int maxLengthName = 20;
        public ChangeColorModelValidator() {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(maxLengthName);
        }
    }
}
