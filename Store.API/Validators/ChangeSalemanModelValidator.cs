﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class ChangeSalesmanModelValidator: AbstractValidator<ChangeSalesmanModel>
    {
        private const short minExperience = 1;
        private const short maxExperience = 40;
        private const uint minSalary = 0;
        private const int maxLengthName = 30;
        public ChangeSalesmanModelValidator() {
            RuleFor(x => x.Experience).NotEmpty().GreaterThanOrEqualTo(minExperience).LessThanOrEqualTo(maxExperience);
            RuleFor(x => x.Name).NotEmpty().MaximumLength(maxLengthName);
            RuleFor(x => x.Salary).NotEmpty().GreaterThanOrEqualTo(minSalary);
        }
    }
}
