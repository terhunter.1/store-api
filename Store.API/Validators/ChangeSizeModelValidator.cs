﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class ChangeSizeModelValidator: AbstractValidator<ChangeSizeModel>
    {
        private const int maxLengthName = 20;
        public ChangeSizeModelValidator() {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(maxLengthName);
        }
    }
}
