﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class AuthorizationModelValidator: AbstractValidator<AuthorizationModel>
    {
        private const int maxLengthLogin = 20;
        private const int maxLengthPassword = 16;
        public AuthorizationModelValidator() {
            RuleFor(x => x.Login).NotEmpty().MaximumLength(maxLengthLogin);
            RuleFor(x => x.Password).NotEmpty().MaximumLength(maxLengthPassword);
        }
    }
}
