﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class ChangeMerchandiseModelValidator: AbstractValidator<ChangeMerchandiseModel>
    {
        private const int maxLengthName = 20;
        private const int minId = 1;
        private const uint minStockQuantity = 0;
        public ChangeMerchandiseModelValidator() {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(maxLengthName);
            RuleFor(x => x.CategoryId).NotEmpty().GreaterThanOrEqualTo(minId);
            RuleFor(x => x.ColorId).NotEmpty().GreaterThanOrEqualTo(minId);
            RuleFor(x => x.SizeId).NotEmpty().GreaterThanOrEqualTo(minId);
            RuleFor(x => x.StockQuantity).NotEmpty().GreaterThanOrEqualTo(minStockQuantity);
        }
    }
}
