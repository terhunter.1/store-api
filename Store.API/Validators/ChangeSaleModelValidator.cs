﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class ChangeSaleModelValidator: AbstractValidator<ChangeSaleModel>
    {
        private const int minId = 1;
        private const uint minQuantity = 0;
        public ChangeSaleModelValidator() {
            RuleFor(x => x.MerchandiseId).NotEmpty().GreaterThanOrEqualTo(minId);
            RuleFor(x => x.SalesmanId).NotEmpty().GreaterThanOrEqualTo(minId);
            RuleFor(x => x.Quantity).NotEmpty().GreaterThanOrEqualTo(minQuantity);
        }
    }
}
