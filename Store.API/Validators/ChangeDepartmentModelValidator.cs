﻿using Store.API.Models;
using FluentValidation;

namespace Store.API.Validators
{
    public class ChangeDepartmentModelValidator: AbstractValidator<ChangeDepartmentModel>
    {
        private const int maxLengthName = 20;
        public ChangeDepartmentModelValidator() {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(maxLengthName);
        }
    }
}
