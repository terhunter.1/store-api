﻿namespace Store.API.Models
{
    public class ChangeSaleModel
    {
        public int MerchandiseId { get; set; }
        public int SalesmanId { get; set; }
        public uint Quantity { get; set; }
    }
}
