﻿namespace Store.API.Models
{
    public class ChangeSalesmanModel
    {
        public string Name { get; set; }
        public short Experience { get; set; }

        public uint Salary { get; set; }
    }
}
