﻿namespace Store.API.Models
{
    public class ChangeMerchandiseModel
    {
        public string Name { get; set; }
        public int SizeId { get; set; }
        public int ColorId { get; set; }
        public int CategoryId { get; set; }
        public uint StockQuantity { get; set; }
    }
}
