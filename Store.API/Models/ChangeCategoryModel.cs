﻿namespace Store.API.Models
{
    public class ChangeCategoryModel
    {
            public int DepartmentId { get; set; }
            public string Name { get; set; }
        }
}
